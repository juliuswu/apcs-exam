#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
void wash_poker(int a[], int num)
{
    int t[201];
    int i,j;
    
    memcpy(t, a, sizeof(t));
    j=1;
    for(i=1;i<=num;i=i+2)
    {
        a[j]=t[i];
        a[j+num/2]=t[i+1];
        j++;
    }
    return;
}

void r_wash_poker(int a[], int num)
{
    int t[201];
    int i,j;
    
    memcpy(t, a, sizeof(t));
    j=1;
    for(i=1;i<=num;i=i+2)
    {
        a[i]=t[j];
        a[i+1]=t[j+num/2];
        j++;
    }
    return;
}

void k_wash_poker(int a[], int num, int k)
{
    int t[201];
    int i;
    
    memcpy(t, a, sizeof(t));
    for(i=1;i<=num;i=i+1)
    {
        if(i<=(num-k))
            a[i+k]=t[i];
        else
            a[i-(num-k)]=t[i];
    }
    return;
    
}


int main(int argc, char *argv[]) {
    int n, m;
    int poker[201];
    int process[36];
    int i, j;
    
    scanf("%d %d", &n, &m);
    for(i=1;i<=n;i++)
    {
        scanf("%d", &poker[i]);
    }
    for(i=1;i<=m;i++)
    {
        scanf("%d", &process[i]);
    }

#if 0
    printf("=============================\n");
    printf("n:%d, m:%d.\n", n, m);
    for(j=1;j<=n;j++)
    {
        if(j<n)
            printf("%d ", poker[j]);
        else
            printf("%d\n", poker[j]);
    }
    for(j=1;j<=m;j++)
    {
        if(j<m)
            printf("%d ", process[j]);
        else
            printf("%d\n", process[j]);
    }
    printf("=============================\n");
#endif    

    for(i=m;i>=1;i--)
    {
        switch (process[i])
        {
            case 1:
                wash_poker(poker, n);
                break;
            case 2:
                r_wash_poker(poker, n);
                break;
            default:
                k_wash_poker(poker, n, process[i]);
                break;
        }
    }
    
    for(j=1;j<=n;j++)
    {
        if(j<n)
            printf("%d ", poker[j]);
        else
            printf("%d\n", poker[j]);
    }
      
	return 0;
}
