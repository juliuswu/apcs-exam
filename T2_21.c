#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int k = 4;
    int m = 1;
    
    for(int i=1;i<=5;i++)
    {
        for(int j=1;j<=k;j++)
        {
            printf(" ");
        }
        for(int j=1;j<=m;j++)
        {
            printf("*");
        }
        printf("\n");
        k-=1;
        m+=1;
    }
	return 0;
}
