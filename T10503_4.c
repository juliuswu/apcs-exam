#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int a[101],b[101];
    
    for (int i=1; i<=100; i=i+1) {
        b[i] = i;
        printf("[%04d:%04d]",i, b[i]);
        if(i%5==0)
            printf("\n");
    }
    printf("---------------------------------------------------\n");
    a[0] = 0;
    for (int i=1; i<=100; i=i+1) {
        a[i] = b[i] + a[i-1];
        printf("[%04d:%04d]", i, a[i]);
        if(i%5==0)
            printf("\n");
    }
    printf("---------------------------------------------------\n");
    printf ("%d\n", a[50]-a[30]);
	return 0;
}
