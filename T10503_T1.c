#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void sorting(int *arr, int num)
{
    //int len = sizeof(arr)/sizeof(arr[0]);
    int i, j, temp;
    
    //printf("%s --> size=%d, num=%d\n", __FUNCTION__, len, num);
    //printf("%s--> size=%d\n", __FUNCTION__, num);
    
    //for(int k=0;k<num;k++)
    //{
    //    printf("[%d]", arr[k]);
    //}
    //printf("<--%s\n",__FUNCTION__);
    
    for(i=0;i<num;i++)
    {
        for(j=0;j<num-1-i;j++)
        {
            if(arr[j]>arr[j+1])
            {
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
}
int main(int argc, char *argv[]) {
    int num;
    int *score;
    //int score[21];
    int i=0;
    
    scanf("%d",&num);
    fflush(stdin);
    score = malloc(sizeof(int)*num);
        
    for(i=0;i<num;i++)
    {
        scanf("%d", score+i);
    }
    
    fflush(stdin);
    //printf("i=%d\n",i);
    sorting(score, num);
    
    for(int k=0;k<=num-1;k++)
    {
        if(k==0)
            printf("[");
        if(k!=num-1)
            printf("%d ", *(score+k));
        else
            printf("%d", *(score+k));
    }
    printf("]\n");
    
    if(score[0]>=60)
    {
        printf("best case\n");
        printf("%d\n",score[0]);
    }
    else if(score[num-1]<60)
    {
        printf("%d\n",score[num-1]);
        printf("worst case\n");
    }
    else
    {
        for(int j=num-1;j>=0;j--)
        {
            if(score[j]<60)
            {
                printf("%d\n",score[j]);
                break;
            }
        }
        for(int j=0;j<=num-1;j++)
        {
            if(score[j]>=60)
            {
                printf("%d\n",score[j]);
                break;
            }
        }
    }
     
    free(score);    
	return 0;
}
