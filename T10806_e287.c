#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct road
{
    int dist;
    int pass;
}_ROAD_;

typedef struct neighbor
{
    int n;
    int m;
    int dist;
    //int up;
    //int down;
    //int left;
    //int right;
}_NEIGHBOR_;

int find_minimum(_ROAD_ road[100][100], int n, int m, int *y, int *x)
{
    int i,j;
    int minI=0, minJ=0;
    int minVal=-1;

    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            if(i==0 && j==0)
            {
                minVal=road[i][j].dist;
                minI=i;
                minJ=j;
            }
            else
            {
                if(minVal>road[i][j].dist)
                {
                    minVal=road[i][j].dist;
                    minI=i;
                    minJ=j;
                }                
            }
        }
    }
    *y=minI;
    *x=minJ;
    return minVal;
}

int main(int argc, char *argv[]) {
    _ROAD_ road[100][100];
    _NEIGHBOR_ neighbor[4];
    _NEIGHBOR_ mini_neighbor;
    //int neighbor[4]; 
    int n=0, m=0;
    int i, j;
    int beginVal;
    int beginN, beginM;
    int total_road, cnt_road=1;
    int total_dist=0;
    
    for(i=0;i<100;i++)
    {
        for(j=0;j<100;j++)
        {
            road[i][j].dist=-1;
            road[i][j].pass=0;
        }
    }
    scanf("%d %d",&n,&m);
    total_road=n*m;
    
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            scanf("%d", &road[i][j].dist);
        }
    }
    beginVal=find_minimum(road, n, m, &beginN, &beginM);
    
    //printf("Begin val:%d, N:%d, M:%d\n", beginVal, beginN, beginM);
    
    if(beginVal<0)
    {
        printf("Something wrong\n");
        return 0;
    }
    
    total_dist+=beginVal;
    road[beginN][beginM].pass=1;
    
    while(cnt_road<total_road)
    {
        neighbor[0].n=neighbor[1].n=neighbor[2].n=neighbor[3].n=0;
        neighbor[0].m=neighbor[1].m=neighbor[2].m=neighbor[3].m=0;
        neighbor[0].dist=neighbor[1].dist=neighbor[2].dist=neighbor[3].dist=-1;
        if(beginN-1>=0)
        {
            if(road[beginN-1][beginM].pass==0)
            {
                neighbor[0].n = beginN-1;
                neighbor[0].m = beginM;
                neighbor[0].dist = road[beginN-1][beginM].dist;
            }
        }
        if(beginN+1<n)
        {
            if(road[beginN+1][beginM].pass==0)
            {
                neighbor[1].n = beginN+1;
                neighbor[1].m = beginM;
                neighbor[1].dist = road[beginN+1][beginM].dist;
            }
        }
        if(beginM-1>=0)
        {
            if(road[beginN][beginM-1].pass==0)
            {
                neighbor[2].n = beginN;
                neighbor[2].m = beginM-1;
                neighbor[2].dist = road[beginN][beginM-1].dist;
            }
        }
        if(beginM+1<m)
        {
            if(road[beginN][beginM+1].pass==0)
            {
                neighbor[3].n = beginN;
                neighbor[3].m = beginM+1;
                neighbor[3].dist = road[beginN][beginM+1].dist;
            }
        }
        if(neighbor[0].dist==-1 && neighbor[1].dist==-1 && neighbor[2].dist==-1 && neighbor[3].dist==-1)
        {
            //printf("Total distance:%d\n", total_dist);
            //printf("%d\n",total_dist);
            break;
        }
        //printf("Up: %d %d [%d]\n", neighbor[0].n, neighbor[0].m, neighbor[0].dist);
        //printf("Down: %d %d [%d]\n", neighbor[1].n, neighbor[1].m, neighbor[1].dist);
        //printf("Left: %d %d [%d]\n", neighbor[2].n, neighbor[2].m, neighbor[2].dist);
        //printf("Right: %d %d [%d]\n", neighbor[3].n, neighbor[3].m, neighbor[3].dist);
        
        mini_neighbor.n=0;
        mini_neighbor.m=0;
        mini_neighbor.dist=-1;
        
        for(i=0;i<4;i++)
        {
            if((mini_neighbor.dist==-1 || (mini_neighbor.dist>neighbor[i].dist)) && (neighbor[i].dist!=-1))
            {
                mini_neighbor.dist=neighbor[i].dist;
                mini_neighbor.n=neighbor[i].n;
                mini_neighbor.m=neighbor[i].m;
            }
        }
        
            
        total_dist+=mini_neighbor.dist;
        beginN=mini_neighbor.n;
        beginM=mini_neighbor.m;
        road[beginN][beginM].pass=1;
        //printf("Total val:%d, N:%d, M:%d\n", total_dist, beginN, beginM);
        
        cnt_road++;
    }
    printf("%d\n",total_dist);
    
#if 0   
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            printf("[%6d]", road[i][j].dist);
        }
        printf("\n");
    }
#endif    
    
    
    
	return 0;
}
