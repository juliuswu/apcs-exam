#!/usr/bin/env python3

m,n = map(int,input().split())
lst = input()
clst = []
num=0

for i in range(0,n-m+1):
	if i==0:
		clst=lst.split(" ",m)
	else:
		clst.pop(0)
		clst.extend(lst.split(" ",1))
		
	if len(clst)>m:
		lst=clst.pop(m)
	else:
		lst=clst
	if len(set(clst))==m:
		num=num+1

print(num)
