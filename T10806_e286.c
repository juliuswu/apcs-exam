#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int score[4][4];
    int game[2][2];
    char *result[3]={"Win", "Lose", "Tie"};
    
    int i, j, k;
    
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            scanf("%d", &score[i][j]);
        }
    }
    
    for(i=0;i<2;i++)
    {
        for(j=0;j<2;j++)
        {
            game[i][j]=0;
            for(k=0;k<4;k++)
            {
                game[i][j]+=score[j+i*2][k];
            }
        }
    }
        
    for(i=0;i<2;i++)
    {
        printf("%d:%d\n",game[i][0],game[i][1]);
    }
    
    if((game[0][0]>game[0][1]) && (game[1][0]>game[1][1]))
    {
        printf("%s\n", result[0]);
    }
    else if((game[0][0]<game[0][1]) && (game[1][0]<game[1][1]))
    {
        printf("%s\n", result[1]);
    }
    else
    {
        printf("%s\n", result[2]);
    }
    
#if 0
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            printf("[%d]",score[i][j]);
        }
        printf("\n");
    }
    
    for(i=0;i<2;i++)
    {
        for(j=0;j<2;j++)
        {
            printf("[%d]",game[i][j]);
        }
        printf("\n");
    }
#endif
    
	return 0;
}
