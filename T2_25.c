#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int i = 321;
    int j = 123;
    int k;
    
    while((i%j)!=0)
    {
        k=i%j;
        i=j;
        j=k;
    }
    printf("%d\n", j);
	return 0;
}
