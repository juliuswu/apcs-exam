#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void BubbleSort(int arr[], int len) 
{
	int i, j, temp;
	for (i = 0; i < len - 1; ++i){          //循環N-1次
		for (j = 0; j < len - 1 - i; ++j){  //每次循環要比較的次數
	       for (int k = 0; k < len; ++k){
		      printf("%d ", arr[k]);
            }
		    printf(" <== (%d,%d)\n",i,j);
			if (arr[j] > arr[j + 1])       //比大小後交換
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int i;
	int arr[] = {5,4,9,8,7,6,14,11,12,1,2,3,10,13};
	int len=sizeof(arr)/sizeof(arr[0]);
	
	BubbleSort(arr, len);
	
	for (i = 0; i < len; ++i)
		printf("%d ", arr[i]);
	printf("\nLength:%d\n",len);
    return 0;
}
