#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
void foo(int i);
void bar(int i);

void foo (int i) {
    printf("%s(%d)\n",__FUNCTION__,i);
    if (i <= 5) {
        printf ("foo: %d\n", i);
    }
    else {
        bar(i - 10);
    }
    return;
}
void bar (int i) {
    printf("%s(%d)\n",__FUNCTION__,i);
    if (i <= 10) {
        printf ("bar: %d\n", i);
    }
    else {
        foo(i - 5);
    }
    return;
}

int main(int argc, char *argv[]) {
    //foo(15106);
    //bar(91);
    foo(93);
	return 0;
}
