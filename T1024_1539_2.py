#!/usr/bin/env python3

m,n = map(int,input().split())
lst = input().split()
num=0

for i in range(0,n-m+1):
	if len(set(lst[i:i+m-1]))==m:
		num=num+1

print(num)
