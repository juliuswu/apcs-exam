#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void cal(char s[])
{
    int i=0,j=0;
    int len;
    int begin=0;
    int is_left=1;
    int is_minus=0;
    char temp[5];
    int lg_val=0, lx_val=0;
    int rg_val=0, rx_val=0;
     
    len=strlen(s);   
        
    memset(temp, 0x00, sizeof(temp));
    
    for(i=0;i<len;i++)
    {
        //printf("%d => s[%d]:%c\n", begin, i, s[i]);
        switch(s[i])
        {
            case '0': case '1': case '2':
            case '3': case '4': case '5':
            case '6': case '7': case '8':
            case '9':
                temp[j]=s[i];
                j++;
                break;
            case 'x':
                if(begin!=i)
                {
                    if(is_left)
                        lx_val+=atoi(temp)*(is_minus==1?-1:1);
                    else
                        rx_val+=atoi(temp)*(is_minus==1?-1:1);
                    //printf("%dx\n", cur2->num);
                }
                else
                {
                    if(is_left)
                        lx_val+=1*(is_minus==1?-1:1);
                    else
                        rx_val+=1*(is_minus==1?-1:1);
                    //printf("%dx\n", cur2->num);
                }
                begin=i;
                j=0;
                memset(temp, 0x00, sizeof(temp));
                break;
            case '-': case '+': case '=':
                if(begin!=i)
                {
                    if(is_left)
                        lg_val+=atoi(temp)*(is_minus==1?-1:1);
                    else
                        rg_val+=atoi(temp)*(is_minus==1?-1:1);
                    //printf("%d\n", cur1->num);
                }
                begin=i+1;
                if(s[i]=='-')
                    is_minus=1;
                else
                    is_minus=0;
                if(s[i]=='=')
                    is_left=0;
                j=0;
                memset(temp, 0x00, sizeof(temp));
                break;
        }
    }    
    
    if(begin!=i)
    {
        if(is_left)
            lg_val+=atoi(temp)*(is_minus==1?-1:1);
        else
            rg_val+=atoi(temp)*(is_minus==1?-1:1);
        //printf("%d\n", cur1->num);
    }
    //printf("%s[%d]\n", s, len);
    
    //printf("lg_val:%d, lx_val:%d,\n", lg_val, lx_val);
    //printf("rg_val:%d, rx_val:%d,\n", rg_val, rx_val);
    if(lx_val==rx_val && lg_val!=rg_val)
        printf("IMPOSSIBLE\n");
    else if(lx_val==rx_val && lg_val==rg_val)
        printf("IDENTITY\n");
    else 
    {
        printf("%d\n", (int)floor((double)(rg_val-lg_val)/(lx_val-rx_val)));
    }
}

int main(int argc, char *argv[]) {
    int t;
    int i;
    char str_t[10][256];
    
    scanf("%d", &t);
    for(i=0;i<t;i++)
    {
        scanf("%s", str_t[i]);
    }
    
#if 0
    printf("t:%d\n", t);
#endif
    for(i=0;i<t;i++)
    {
        cal(str_t[i]);
    }
	return 0;
}



#if 0
#include <stdio.h>
#include <math.h>
void parse(char *s, int& A, int& B) {
    A = 0, B = 0;
    int i, g = 0, f = 0, neg = 1;
    for(i = 0; s[i]; i++) {
        if(s[i] == 'x') {
            if(g)
                A += neg*f;
            else
                A += neg;
            g = 0, f = 0, neg = 1;
        } else {
            if(s[i] == '-') {
                if(g)
                    B += neg*f;
                g = 0, f = 0;
                neg = -1;
            } else if(s[i] == '+') {
                if(g)
                    B += neg*f;
                g = 0, f = 0;
                neg = 1;
            } else
                f = f*10 + s[i]-'0', g = 1;
        }
    }
    if(g)
        B += neg*f;
}
int main() {
    int t, i;
    scanf("%d", &t);
    char s1[502], *s2;
    while(t--) {
        scanf("%s", s1);
        for(i = 0; s1[i]; i++) {
            if(s1[i] == '=') {
                s2 = s1+i+1;
                s1[i] = '\0';
                break;
            }
        }
        int la, lb, ra, rb;
        parse(s1, la, lb);
        parse(s2, ra, rb);
        if(la == ra && lb == rb)
            puts("IDENTITY");
        else if(la == ra && lb != rb)
            puts("IMPOSSIBLE");
        else
            printf("%d\n", (int)floor((double)(rb-lb)/(la-ra)));
    }
    return 0;
}
#endif
