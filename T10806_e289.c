#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define LL long long
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
LL intsrch(LL A[], LL find, LL len)
{
    LL low, mid, high;//,Searchtime;
    low = 0;

    high = len - 1;
    //Searchtime = 0;

    while(low <= high)
    {
        if(low==high)
            mid = high;
        else if(A[high]==A[low])
            mid = low;
        else
            mid = (high-low)* (find-A[low])/(A[high]-A[low])+ low;
        //printf("low:%lld, mid:%lld, high:%lld\n", low, mid, high);
        //Searchtime = Searchtime + 1;   
        if(mid < low || mid > high)  return -1;
        if(find < A[mid])
            high = mid - 1;
        else if(find > A[mid])
            low = mid + 1;
        else
        {
            //printf("一共搜尋 %d 次, ",Searchtime);//顯示搜尋次數
            //printf("find:%lld, mid:%lld\n", find, mid);
            return mid;
        }
    }
    return -1;
}

LL insertion_sort(LL poker[], LL n){
  LL i, j, card;
  LL dup=0;
  for(i = 1; i < n; i++){ //從第二張牌開始跟前面的撲克牌比大小
    card = poker[i];
    for(j = i-1; j>=0 && card<=poker[j]; j--){ //把card插入前面排序好的撲克牌
      if(card==poker[j])
        dup++;
      poker[j+1] = poker[j];
      
    }
    poker[j+1] = card;
  }
  return dup;
}

LL beauty(LL i, LL j, LL a[], LL aux[]) {
    LL dup=0;
    if (j <= i) {
        return 0;     // the subsection is empty or a single element
    }
    LL mid = (i + j) / 2;

    // left sub-array is a[i .. mid]
    // right sub-array is a[mid + 1 .. j]
    
    dup+=beauty(i, mid, a, aux);     // sort the left sub-array recursively
    dup+=beauty(mid + 1, j, a, aux);     // sort the right sub-array recursively

    LL pointer_left = i;       // pointer_left points to the beginning of the left sub-array
    LL pointer_right = mid + 1;        // pointer_right points to the beginning of the right sub-array
    LL k;      // k is the loop counter

    // we loop from i to j to fill each element of the final merged array
    for (k = i; k <= j; k++) {
        if (pointer_left == mid + 1) {      // left pointer has reached the limit
            aux[k] = a[pointer_right];
            pointer_right++;
        } else if (pointer_right == j + 1) {        // right pointer has reached the limit
            aux[k] = a[pointer_left];
            pointer_left++;
        } else if (a[pointer_left] < a[pointer_right]) {        // pointer left points to smaller element
            aux[k] = a[pointer_left];
            pointer_left++;
        } else {        // pointer right points to smaller element
            if(a[pointer_left] == a[pointer_right])
                dup++;
            aux[k] = a[pointer_right];
            pointer_right++;
        }
    }

    for (k = i; k <= j; k++) {      // copy the elements from aux[] to a[]
        a[k] = aux[k];
    }
    return dup;
}

int main(int argc, char *argv[]) {
    long long m, n, i, j;
    //long long list[200001];
    //long long aux[200001];
    long long *list;
    long long *temp;
    long long *aux;
    LL dup=0;
    //LL max_level=0;
    LL num_beauty=0;
    //long long *aux;
    
    scanf("%lld %lld", &m, &n);
    
    list=(long long*) malloc (sizeof(long long)*n);
    temp=(long long*) malloc (sizeof(long long)*m);
    aux=(long long*) malloc (sizeof(long long)*m);
    
    for(i=0;i<n;i++)
    {
        scanf("%lld", &list[i]);
        if(i>=(m-1))
        {
            if(i==(m-1))
            {
                j=0;
                memcpy(temp, list+j, sizeof(long long)*m);
                dup=beauty(0, m-1, temp, aux);
            }
            else
            {
                LL sch_id;
                sch_id=intsrch(temp, list[j-1], m);
                temp[sch_id]=list[i];
                dup=insertion_sort(temp, m);
            }
#if 0
            printf("[%lld]dup:%lld\n", j, dup);
#endif
            if(dup==0)
                num_beauty++;
            //if(max_level<(m-dup))
            //    max_level=m-dup;
            j++;
        }        
    }
    //printf("%lld\n", max_level);
    printf("%lld\n", num_beauty);
#if 0
    printf("m:%lld, n:%lld\n", m, n);
    for(i=0;i<n;i++)
    {
        printf("%lld", list[i]);
    }
    printf("\n");
    //printf("max_level:%lld\n", max_level);
    
    for(i=0;i<(n-m+1);i++)
    {
        for(j=0;j<m;j++)
        {
            printf("%lld", list[i+j]);
        }
        printf("\n");
    }
#endif
    
#if 0
    printf("m:%lld, n:%lld\n", m, n);
    
    for(i=0;i<n;i++)
    {
        printf("%lld", list[i]);
    }
#endif
	return 0;
}
