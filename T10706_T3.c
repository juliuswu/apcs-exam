#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
typedef struct process_info
{
    int use_machine;
    int use_duration;
}_PROCESS_INFO_;

typedef struct job_info
{
    int num_process;
    int cur_process;
    int max_duration;
    _PROCESS_INFO_ process[100];
}_JOB_INFO_;

typedef struct machine_info
{
    int duration;
}_MACHINE_INFO_;

int main(int argc, char *argv[]) {
    
    _JOB_INFO_ job[100];
    _MACHINE_INFO_ machine[100];
    
    int total_process=0;
    int cnt_process;
    
    int num_machine=0;
    int num_job=0;
    int i, j;
    
    int total_duration=0;
    
    scanf("%d %d", &num_machine, &num_job);
    fflush(stdin);
    
    if(num_machine>=100 || num_job>=100)
    {
        printf("over 100\n");
        return 0;
    }
    
    for(i=0;i<num_machine;i++)
    {
        machine[i].duration=0;
    }
    
    for(i=0;i<num_job;i++)
    {
        job[i].max_duration=0;
        scanf("%d", &job[i].num_process);
        total_process+=job[i].num_process;
        job[i].cur_process=0;
        for(j=0;j<job[i].num_process;j++)
        {
            scanf("%d %d", &job[i].process[j].use_machine, &job[i].process[j].use_duration);
            
            if(job[i].process[j].use_machine>=num_machine)
            {
                printf("over mahcine number\n");
                return 0;
            }
        }
        
    }
#if 1
    printf("==============JOB INFOMATION==============\n");   
    printf("Machine:%d, Job:%d\n", num_machine, num_job);
    printf("Total Process:%d\n", total_process);
    for(i=0;i<num_job;i++)
    {
        printf("Process %d\n", job[i].num_process);
        printf("Current Process %d\n", job[i].cur_process);
        printf("Max Duration %d\n", job[i].max_duration);
        for(j=0;j<job[i].num_process;j++)
        {
            printf("%d %d\n", job[i].process[j].use_machine, job[i].process[j].use_duration);
        }
    }
    printf("==========================================\n"); 
#endif
#if 1
    cnt_process=0;
    while(cnt_process<total_process)
    {
        int next_job=-1;
        int next_machine=-1;
        int next_duration=-1;
        int temp_process;
        int temp_machine;
        int temp_duration;
        int temp_max_duration;
        int temp_machine_duration;
        
        for(i=0;i<num_job;i++)
        {
			temp_process=job[i].cur_process;
            
            if(temp_process==job[i].num_process)
                continue;
                
            temp_machine=job[i].process[temp_process].use_machine;
            temp_duration=job[i].process[temp_process].use_duration;
            temp_max_duration=job[i].max_duration;
            temp_machine_duration=machine[temp_machine].duration;
            
            if(next_duration==-1)
            {
                next_job=i;
                next_machine=temp_machine;
                if(temp_max_duration>=temp_machine_duration)
                {
                    next_duration=temp_duration+temp_max_duration;
                }
                else
                {
                    next_duration=temp_duration+temp_machine_duration;
                }
            }
            else
            {
                if(temp_max_duration>=temp_machine_duration)
                {
                    if(next_duration>temp_duration+temp_max_duration)
                    {
                        next_job=i;
                        next_machine=temp_machine;
                        next_duration=temp_duration+temp_max_duration;
                    }
                }
                else
                {
                    if(next_duration>temp_duration+temp_machine_duration)
                    {
                        next_job=i;
                        next_machine=temp_machine;
                        next_duration=temp_duration+temp_machine_duration;
                    }
                }
            }
        }
        job[next_job].cur_process++;
        job[next_job].max_duration=next_duration;
        machine[next_machine].duration=next_duration;
        
        printf("Job %d current Process:%d\n",next_job, job[next_job].cur_process);
        printf("%d: job:%d, machine:%d, Machine duration:%d, Max Duration:%d\n", cnt_process, next_job, next_machine, machine[next_machine].duration, job[next_job].max_duration);
        cnt_process++;
    }
#endif
    for(i=0;i<num_job;i++)
    {
        printf("Job %d : %d\n", i, job[i].max_duration);
        total_duration+=job[i].max_duration;
    }   
    printf("Total Job Duration:%d\n", total_duration);
	return 0;
}
