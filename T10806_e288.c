#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <memory.h>


/* run this program using the console pauser or add your own getch, system("pause") or input loop */
/* A~Z: 65~90, a~l: 97~108 */

typedef struct cp{
    //char *str;
    int str_len;
    long long int bin;
    long long int cp_bin;
}CP;

#define MAX_CP_NUM 0x3FFFFFFFFF


//#include "hash_table.h"

typedef struct {
    char* key;
    char* value;
} ht_item;

typedef struct {
    int size;
    int count;
    ht_item** items;
} ht_hash_table;

static ht_item* ht_new_item(const char* k, const char* v);
ht_hash_table* ht_new();
static void ht_del_item(ht_item* i);
void ht_del_hash_table(ht_hash_table* ht);

static ht_item* ht_new_item(const char* k, const char* v) {
    ht_item* i = malloc(sizeof(ht_item));
    i->key = strdup(k);  // �ƻs�ާ@
    i->value = strdup(v);
    return i;
}

ht_hash_table* ht_new() {
    ht_hash_table* ht = malloc(sizeof(ht_hash_table));

    ht->size = 53;
    ht->count = 0;
    ht->items = calloc((size_t)ht->size, sizeof(ht_item*));
    return ht;
}

static void ht_del_item(ht_item* i) {
    free(i->key);
    free(i->value);
    free(i);
}


void ht_del_hash_table(ht_hash_table* ht) {
    for (int i = 0; i < ht->size; i++) {
        ht_item* item = ht->items[i];
        if (item != NULL) {
            ht_del_item(item);
        }
    }
    free(ht->items);
    free(ht);
}

int main(int argc, char *argv[]) {
    CP *cp_str;
    char *str;
    int num_role;
    long long int num_cp;
    long long int i,j,k;
    long long int cp_mask;
    long long int cp_paired=0;
    int id;
    
#if 0
#if 0    
    long long int cp1, cp2;    
    cp1=0x1234567890;    
    cp2=pow(2, 38);//-(cp1&0x3fffffffff);    
    printf("%010llX\n",cp2);
    printf("%010llX\n",cp2-(cp1&MAX_CP_NUM)-1);
    
    printf("%d, %d\n", sizeof(double), sizeof(long long int));
#endif    
    ht_hash_table* ht = ht_new();
    ht_del_hash_table(ht);
    return 0;
#endif
    
    scanf("%d %lld", &num_role, &num_cp);
    
    //printf("%lld", num_cp);
    
    cp_mask=pow(2, num_role)-1;
    
    cp_str=(CP *)malloc(sizeof(CP)*num_cp);
    str=(char*)malloc(sizeof(char)*1001);
    
    for(i=0;i<num_cp;i++)
    {
        cp_str[i].str_len=0;
        cp_str[i].bin = 0;
        cp_str[i].cp_bin = cp_mask - cp_str[i].bin;
    }
    
    for(i=0;i<num_cp;i++)
    {
        scanf("%s", str);
        cp_str[i].str_len=(int)strlen(str);
        for(j=0;j<cp_str[i].str_len;j++) 
        {
            id=0;
            if(str[j]>=65 && str[j]<=90)
            {
                id = (int)(str[j])-65;
            }
            else if(str[j]>=97 && str[j]<=108)
            {
                id = (int)(str[j])-65-(97-90+1);             
            }
            else
            {
                printf("Error!!\n");
                free(cp_str); 
	            return 0;
            }
            cp_str[i].bin |= (1<<id)&cp_mask;
            cp_str[i].cp_bin = cp_mask - cp_str[i].bin;  
        }
#if 0
        for(k=0;k<i;k++)
        {
            if(cp_str[k].cp_bin==cp_str[i].bin)
            {
                cp_paired++;
            }
        }
        //printf("%llx\n", cp_str[i].bin);
        //printf("%llx\n", cp_str[i].cp_bin);
#endif
    }
    printf("%lld\n", cp_paired);
    
#if 0
    printf("Role:%d\n", num_role);
    printf("cp_mask:0x%llx\n", cp_mask);
    for(i=0;i<num_cp;i++)
    {
        printf("%s[%d]\n", cp_str[i].str, (int)strlen(cp_str[i].str));
    }
#endif   

    free(cp_str); 
	return 0;
}
