#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    
    int a=0;
    int n=5;
    int cnt=0;
    
    for(int i=1;i<=n;i++)
    {
        for(int j=i;j<=n;j++)
        {
            for(int k=1;k<=n;k++)
            {
                a++;
                printf("[%d,%d,%d:%03d]\n",i,j,k,a);
                cnt++;
            }
        }
    }
                
    printf("\n%d[%d]\n",a,cnt);
	return 0;
}
