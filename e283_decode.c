#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define CODESIZE 4
#define ul64 unsigned long long int
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
ul64 total=0;

typedef struct lst_code{
    ul64 num_code;
    char *str_code;
    struct lst_code* next;
}_LST_CODE;

char decode(int a[])
{
    char c;
    if(a[0]==0)
    {
        if(a[1]==0)
            c='C';
        else
        {
            if(a[2]==0)
                c='A';
            else
                c='B';
        }
    }
    else
    {
        if(a[1]==0)
            c='E';
        else
        {
            if(a[3]==0)
                c='F';
            else
                c='D';
        }
    }
    return c;
}
int main(int argc, char *argv[]) {
    
    //_LST_CODE *lcode = (_LST_CODE *) calloc(total, sizeof(_LST_CODE));
    //lcode[total].str_code = (char *)calloc(total, sizeof(char));
    //_LST_CODE *lcode;
    //lcode = new _LST_CODE;
    
    //_LST_CODE *current = lcode;
    
    //_LST_CODE *t;
    //char *b;
    _LST_CODE *lcode = (_LST_CODE *) malloc (sizeof(_LST_CODE));
    
    _LST_CODE *current = lcode;
    _LST_CODE *head = lcode;
    
    ul64 num=0;
    ul64 i, j;
    char code;
    int r[CODESIZE];
    int k; 
    int ret;   
    
    while(1)
    {
        //printf("ret:%d\n",ret);
        num=0;
        memset(r, -1, sizeof(r));
        ret=scanf("%llu", &num);
        if(ret==EOF || ret!=1)
        {
            //printf("1st break\n");
            break;
        }            
        //printf("1 num:%llu, total:%llu,\n", num, total);  
        total++;  
        if(total>1)
        {
            current->next = (_LST_CODE *) malloc (sizeof(_LST_CODE));
            current = current->next;
        }
        char *b = (char *) calloc(num+1, sizeof(char));
        
        current->num_code = num;
        current->str_code = b;
        current->next = NULL;
        
        for(i=0;i<num;i++)
        {
            for(k=0;k<CODESIZE;k++)
            {
                ret=scanf("%d", &r[k]);
                if(ret==EOF || ret!=1)
                {
                    //printf("2nd break\n");
                    break;
                }  
                //printf("r[%d]:%d\n", k, r[k]);              
            }
            code=decode(r);
            current->str_code[i]=code;
        }
        current->str_code[num]='\0';
        //printf("2 num:%llu, total:%llu,\n", num, total);  
        //free(b);
    }
    
    current = head;
    for(j=0;j<total;j++)
    {
        //for(k=0;k<current->num_code;k++)
        {
            printf("%s\n", current->str_code);
        }
        current = current->next;
    }
        
    free(lcode);     
    return 0;
}
