#!/usr/bin/env python3

m,n = map(int,input().split())
lst = input().split()
clst = []
num=0

for i in range(0,n-m+1):
	if i==0:
		clst=lst[0:m]
	else:
		clst.pop(0)
		clst.append(lst[m-1+i])
	if len(set(clst))==m:
		num=num+1

print(num)

