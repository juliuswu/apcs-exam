#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    
    for(int i=0;i<=3;i++){
        for(int j=0;j<i;j++)
            printf(" ");
        for(int k=6-2*i;k>0;k--)
            printf("*");
        printf("\n");
    }
    printf("-----------------\n");
	return 0;
}
