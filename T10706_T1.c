#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
    int num;
    int e[4][4];
    int i,j,k,l,m;
    
    scanf("%d", &num);
    fflush(stdin);
    for(i=0;i<num;i++)
    {
        for(j=0;j<4;j++)
        {
            scanf("%d", &e[i][j]);
        }
    }
    fflush(stdin);
    
    printf("num=%d\n",num);
    for(k=0;k<num;k++)
    {
        for(l=0;l<4;l++)
        {
            printf("[%d]",e[k][l]);
        }
        printf("\n");
    }
    
    for(m=0;m<num;m++)
    {
        if(e[m][0]==0)
        {
            if(e[m][1]==0)
            {
                if(e[m][2]==1 && e[m][3]==0)
                    printf("C");
                else
                    printf("?");
            }
            else
            {
                if(e[m][2]==0)
                {
                    if(e[m][3]==1)
                        printf("A");
                    else
                        printf("?");
                }
                else
                {
                    if(e[m][3]==1)
                        printf("B");
                    else
                        printf("?");
                }
            }
        }
        else
        {
            if(e[m][1]==0)
            {
                if(e[m][2]==0 && e[m][3]==0)
                    printf("E");
                else
                    printf("?");
            }
            else
            {
                if(e[m][3]==0)
                {
                    if(e[m][2]==0)
                        printf("F");
                    else
                        printf("?");
                }
                else
                {
                    if(e[m][2]==0)
                        printf("D");
                    else
                        printf("?");
                }
            }            
        }
    }
    printf("\n==========================\n");
    
	return 0;
}
